import express from 'express'
import cors from 'cors'
import EventEmitter from 'events'
import MainCaro from './mainCaro.js'
import e from 'express';
const PORT = 8080;
const app = express()
app.use(cors())

app.listen(PORT, () => {
    console.log(`Facts Events service listening at http://localhost:${PORT}`)
  })
class MyEmitter extends EventEmitter {}

const EmitterProcess = new MyEmitter();

const mainCaro = new MainCaro()
app.get("/listRoom", async(req, res) => {
    return res.send(mainCaro.getListRoom())
});
app.get("/listRoom-EventEmit", async(req, res) => {
    res.setHeader('Cache-Control', 'no-cache');
    res.setHeader('Content-Type', 'text/event-stream; charset=utf-8');
    res.setHeader('Connection', 'keep-alive');
    res.setHeader('X-Accel-Buffering', 'no');
    res.flushHeaders();

    function writeData(data){
      res.write(`data: ${JSON.stringify(data)}`);
      res.write('\n\n');
      res.flushHeaders();
    }
    
    writeData(mainCaro.getListRoom());
    
    //keep channel live.
    time= setInterval(()=>{
        const listRemove = mainCaro.removeRooms()
        if(listRemove?.length > 0){
            EmitterProcess.emit('getRoom',mainCaro.getListRoom())
        }
        res.write("REMOVED")        
      res.flushHeaders();
    },117000)

    EmitterProcess.on('getRoom',writeData)
    // listen for channel close events
    res.on('close',function(){
    //   EmitterProcess.off(roomId ,writeData);
    })
})

let time
function pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}
app.get("/attack/:roomId", async(req, res) => {
    const user =req.query.user;
    const roomId = req.params.roomId;
    const x = req.query.x
    const y = req.query.y
    const canCheck = mainCaro.rooms[roomId].canAttach === user
    if(!canCheck) return res.send({status:false,message:'Mày đợi tí'})
    const isExist1 = mainCaro.rooms[roomId].datatrue?.some(v => (+v[0] === +x) && (+v[1] === +y))
    if(isExist1){
        return res.send({status:false,message:'Trùng'})
    }else{
        const isExist2 = mainCaro.rooms[roomId].datafalse?.some(v => (+v[0] === +x) && (+v[1] === +y))
        if(isExist2){
            return res.send({status:false,message:'Trùng'})
        }
    }
    EmitterProcess.emit(roomId,[pad(x),pad(y),user])
    
    mainCaro.rooms[roomId].handleStick(user,[(x),(y)])
    res.send({status:true,X:pad(x),Y:pad(y),playerAttach : user})
})
app.post("/createRoom", async(req, res) => {
    const newRoom = mainCaro.createRoom()
    EmitterProcess.emit('getRoom',mainCaro.getListRoom())
    return res.send({status:true,newRoom})
})
app.get("/listenRoom/:roomId", async(req, res) => {
    const roomId = req.params.roomId
    res.setHeader('Cache-Control', 'no-cache');
    res.setHeader('Content-Type', 'text/event-stream; charset=utf-8');
    res.setHeader('Connection', 'keep-alive');
    res.setHeader('X-Accel-Buffering', 'no');
    res.flushHeaders();

    function writeData(data){
      res.write(`data: ${JSON.stringify(data)}`);
      res.write('\n\n');
      res.flushHeaders();
      
    }
    writeData('Connected');
    const {player} = mainCaro.rooms[roomId].handleJoin();
    if(!player){
        writeData('Disconnected');
        return res.end()
    }
    else {
        writeData({player});   
    }
    writeData(mainCaro.rooms[roomId]);
    
    //keep channel live.
    time= setInterval(()=>{
      res.write('Running');
      res.flushHeaders();
    },117000)

    EmitterProcess.on(roomId,writeData)
    // listen for channel close events
    res.on('close',function(){
        mainCaro.rooms[roomId].handleOut(player)
        if(!mainCaro.rooms[roomId].playertrue && !mainCaro.rooms[roomId].playerfalse){
            mainCaro.rooms[roomId].datatrue = [];
            mainCaro.rooms[roomId].datafalse = [];
            mainCaro.rooms[roomId].canAttach = "true";
            EmitterProcess.emit('getRoom',mainCaro.getListRoom())
        }
    //   EmitterProcess.off(roomId ,writeData);
    })
})

app.get("/resetGame/:roomId", async(req, res) => {
    const roomId = req.params.roomId
    const roomRemoved = mainCaro.resetGame(roomId)
    EmitterProcess.emit(roomId,roomRemoved)
    return res.send({status:true,message:"Reset completed"})
})
mainCaro.createRoom()
mainCaro.createRoom()
mainCaro.createRoom()