
class Room{
    constructor(id) {
        this.id = String(id);
        this.playertrue = null;
        this.playerfalse = null;
        this.datatrue=[];
        this.datafalse=[];
        this.canAttach = 'true';
    }
    handleStick(player,data){
        this['data'+player]= [...this['data'+player],data]
        this.canAttach = player === 'true' ? 'false' : 'true'
    }
    handleJoin(){ 
        // console.log(`${name} has joined ${this.id}`)
        if(this.playertrue && this.playerfalse){
            return false
        }else{
            if(!this.playertrue){
                this.playertrue = "true"
                return {player:"true"}
            }else if(!this.playerfalse){
                this.playerfalse = "false"
                return {player:"false"}
            }else{
                return false
            }
        }
        
    }
    handleOut(player){
        if(!player) return
        this['player'+player] = null
    }

}
 class MainCaro {
    constructor(){
        this.rooms = {};
    }
    getListRoom(){
        let rooms = [];
        for (const [key, value] of Object.entries(this.rooms)) {
            if(!value.playertrue || !value.playerfalse){
                rooms.push({...value,canJoin:true})
            }else{
                rooms.push({...value,canJoin:false})
            }
            
          }
          return rooms
    }
    createRoom(){
        const newRoom = new Room( Object.keys(this.rooms)?.length + 1);
        this.rooms[newRoom.id] = newRoom
        return newRoom
    }
    
    removeRoom(id){
        this.rooms = this.rooms.filter(room => room.id !== id);
        return this
    }
    removeRooms(){
    const listRemove = Object.keys(this.rooms)?.filter(keyRoom => !this.rooms[keyRoom].playertrue && !this.rooms[keyRoom].playerfalse)
    listRemove.forEach(key => delete this.rooms[key])   
    console.log(`Data has been removed`,listRemove.join(","));
    return listRemove
    }
    resetGame(id){
        this.rooms[id].datatrue = []
        this.rooms[id].datafalse = []
        this.rooms[id].canAttach = 'true'
        return this.rooms[id]
    }
}
export default MainCaro